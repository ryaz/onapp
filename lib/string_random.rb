class StringRandom

  def self.random
    "#{gen('A'..'Z', 3)}-#{gen(0..9, 3)}-#{gen('A'..'Z', 3)}-#{gen(0..9, 3)}-#{gen('A'..'Z', 3)}"
  end

  def self.gen(source, length)
    r = []
    length.times {|i| r << source.to_a.sample}
    r.join
  end
end
